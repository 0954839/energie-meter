#include <msp430.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "lib/gpio.h"
#include "lib/i2c.h"
#include "lib/oled.h"
#include "lib/fonts.h"


//Tabel met sinus waarden. Sneller dan berekenen!
const uint8_t sine[] =
{
 0x7,0x8,0xa,0xb,0xc,0xd,0xd,0xe,
 0xe,0xe,0xd,0xd,0xc,0xb,0xa,0x8,
 0x7,0x6,0x4,0x3,0x2,0x1,0x1,0x0,
 0x0,0x0,0x1,0x1,0x2,0x3,0x4,0x6,
};

int T = 0; //int t
int s = 0;
int v = 0;
char T_5[0]; // set char tijd [decimalen]
char V_in_A0[5]; //set char ADC_IN_A0
char mV_in_A0[5];
char amp_A1[5]; //set char ADC_IN_A1
char mamp_A1[5];
char joul_A1[5]; //
char watt_A1[5];
char mwatt_A1[5];
char joul_A1[5];

// int screen value's
int amp = 0;
int tijd = 0;
int V_in = 0;
int mV_in = 0;
int joul = 0;
int mjoul = 0;
int tjoul = 0;
int mamp = 0;
int watt = 0;
int mwatt = 0;


// Global variables
int adc[3] = {0};      //Sets up an array of 10 integers and zero's the values
int adc_A0 = 0;
int adc_A1 = 0;

// Function prototypes

void adc_Setup();
void adc_Sam10();



void main(void) {
    WDTCTL = WDTPW | WDTHOLD;   // Stop watchdog timer

    adc_Setup();                      // Fucntion call for adc_setup
    // installize pin's
    P2DIR &= ~(1<<3);                  // direction for pin
    P2REN |= 1<<3;                     // zet pul up of pul down, 1 is aan, 0 is uit
    P2OUT &= ~(1<<3);                  // kiezen pul up pul down, 1 is up, 0 is down

    P2DIR &= ~(1<<4);                  // direction for pin.
    P2REN |= 1<<4;                     // zet pul up of pul down aan,
    P2OUT &= ~(1<<4);                  // pul up 1, pul down 0
    P2IE |= 1<<4;                      // enable interupt
    P2IES |= 1<<4;                     // kiezen flank, 1 is low to high, 0 is high to low
    P2IFG &= ~(1<<4);                  // flag interupt


    //Zet display aan
    oledInitialize();
    //eventueel flippen
    oledSetOrientation(FLIPPED);
    //begin met leeg scherm
    oledClearScreen();

    uint8_t kolom,n=0;

    while (v==0)
    {
        if (P2IN & (1<<3))
        {
            v = 1;
            amp = 0;
            T = 0;
            V_in = 0;
            mV_in = 0;
            joul = 0;
            mamp = 0;
            adc_A0 = 0;
            adc_A1 = 0;
        }
        else
        {
            v = 0;
            oledPrint(0,6,"start/reset",small);
        }


    while(v)
    {
        n++;
        n=n%32;
        __enable_interrupt();

        //Read_Acc(); // This function reads the ADC and stores the A0, A1, A2 value's
        /* Update de buffer bij elke kolom.
         * Schuif hem op met de aantal iteraties van de
         * while loop modulus 32 zodat we binnen het
         * bereik van de sine-array blijven.
         */
         //schrijf framebuffer naar het oled.

        // value calculation screen'

       V_in = adc_A0  * 19.1919 / 1000; // calculation for V_in 0.0206.... is for v per step  (mili volts)
       mV_in = (((adc_A0 * 0.0191919) - V_in)*1000);
       amp = (((adc_A1 - 60) * 1.91919)/(0.005*121)/1000);     // opamp versterkt 121 keer
       mamp = ((((adc_A1 - 75) * 1.91919)/(0.005*121))- amp);
       watt = (((adc_A0  * 19.1919) * (((adc_A1 - 60) * 1.91919)/(0.005*121)/1000)) / 1000);
       mwatt = ((adc_A0  * 19.1919) * (((adc_A1 - 60) * 1.91919)/(0.005*121)/1000));


       if (amp < 0)
       {
           amp = 0;
       }

       if (mamp < 0)
       {
           mamp = 0;
       }

       if (watt < 0)
       {
           watt = 0;
       }

       if (mwatt < 0)
       {
           mwatt = 0;
       }

       mjoul = (mjoul + mwatt);

       if (mjoul > 999)
       {
           joul = (joul + (mjoul / 1000));
           tjoul = (mjoul / 1000);
           mjoul = mjoul - (tjoul * 1000);
       }

       oledClearScreen();

       oledPrint(0,4,"meting actief",big);

        itoa(T,&T_5);
        oledPrint(0,0,"sec:",small);
        oledPrint(50,0,T_5,small);

        // weergeven op scherm
        if (V_in < 9)
        {                                       //wanneer V_ < 10
            itoa(V_in,&V_in_A0);
            itoa(mV_in,&mV_in_A0);
            oledPrint(0,1,"volt:",small);
            oledPrint(50,1,"0",small);
            oledPrint(55,1,V_in_A0,small);
            oledPrint(60,1,".",small);
            oledPrint(65,1,mV_in_A0,small);
        }

        else
        {                                       // wanner v_in > 10
            itoa(V_in,&V_in_A0);
            itoa(mV_in,&mV_in_A0);
            oledPrint(0,1,"volt:",small);
            oledPrint(50,1,V_in_A0,small);
            oledPrint(60,1,".",small);
            oledPrint(65,1,mV_in_A0,small);
        }

        //weergeven amp op scherm
        if (mamp < 9)
        {
            itoa(amp,&amp_A1);
            itoa(mamp,&mamp_A1);
            oledPrint(0,2,"Amp:",small);
            oledPrint(50,2,"0",small);
            oledPrint(55,2,amp_A1,small);
            oledPrint(60,2,".",small);
            oledPrint(65,2,"0",small);
            oledPrint(70,2,"0",small);
            oledPrint(75,2,mamp_A1,small);
        }

        else if (mamp < 99)
        {
            itoa(amp,&amp_A1);
            itoa(mamp,&mamp_A1);
            oledPrint(0,2,"Amp:",small);
            oledPrint(50,2,"0",small);
            oledPrint(55,2,amp_A1,small);
            oledPrint(60,2,".",small);
            oledPrint(65,2,"0",small);
            oledPrint(70,2,mamp_A1,small);
        }

        else if (mamp < 999)
        {
            itoa(amp,&amp_A1);
            itoa(mamp,&mamp_A1);
            oledPrint(0,2,"Amp:",small);
            oledPrint(50,2,"0",small);
            oledPrint(55,2,amp_A1,small);
            oledPrint(60,2,".",small);
            oledPrint(65,2,mamp_A1,small);
        }
        else if (mamp < 1999)
        {
            mamp = mamp - 1000;
            itoa(amp,&amp_A1);
            itoa(mamp,&mamp_A1);
            oledPrint(0,2,"Amp:",small);
            oledPrint(50,2,"0",small);
            oledPrint(55,2,amp_A1,small);
            oledPrint(60,2,".",small);
            oledPrint(65,2,mamp_A1,small);
        }

        else if (mamp < 2999)
        {
            mamp = mamp - 2000;
            itoa(amp,&amp_A1);
            itoa(mamp,&mamp_A1);
            oledPrint(0,2,"Amp:",small);
            oledPrint(50,2,"0",small);
            oledPrint(55,2,amp_A1,small);
            oledPrint(60,2,".",small);
            oledPrint(65,2,mamp_A1,small);
        }

        else if (mamp < 3999)
        {
            mamp = mamp - 3000;
            itoa(amp,&amp_A1);
            itoa(mamp,&mamp_A1);
            oledPrint(0,2,"Amp:",small);
            oledPrint(50,2,"0",small);
            oledPrint(55,2,amp_A1,small);
            oledPrint(60,2,".",small);
            oledPrint(65,2,mamp_A1,small);
        }

        else if (mamp < 4999)
        {
            mamp = mamp - 4000;
            itoa(amp,&amp_A1);
            itoa(mamp,&mamp_A1);
            oledPrint(0,2,"Amp:",small);
            oledPrint(50,2,"0",small);
            oledPrint(55,2,amp_A1,small);
            oledPrint(60,2,".",small);
            oledPrint(65,2,mamp_A1,small);
        }

        else if (mamp < 5999)
        {
            mamp = mamp - 5000;
            itoa(amp,&amp_A1);
            itoa(mamp,&mamp_A1);
            oledPrint(0,2,"Amp:",small);
            oledPrint(50,2,"0",small);
            oledPrint(55,2,amp_A1,small);
            oledPrint(60,2,".",small);
            oledPrint(65,2,mamp_A1,small);
        }

        // watt weergeven op scherm
        if (watt < 9)
        {
            itoa(watt,&watt_A1);
            oledPrint(0,3,"watt:",small);
            oledPrint(50,3,"0",small);
            oledPrint(55,3,watt_A1,small);
        }

        else
        {
            itoa(watt,&watt_A1);
            oledPrint(0,3,"watt:",small);
            oledPrint(50,3,watt_A1,small);
        }

        if (mwatt < 9)
        {
            itoa(mwatt,&mwatt_A1);
            oledPrint(60,3,".",small);
            oledPrint(65,3,"0",small);
            oledPrint(70,3,"0",small);
            oledPrint(75,3,mwatt_A1,small);
        }

        else if (mwatt < 99)
        {
            itoa(mwatt,&mwatt_A1);
            oledPrint(60,3,".",small);
            oledPrint(65,3,"0",small);
            oledPrint(70,3,mwatt_A1,small);
        }

        else if(mwatt < 999)
        {
            itoa(mwatt,&mwatt_A1);
            oledPrint(60,3,".",small);
            oledPrint(65,3,mwatt_A1,small);
        }

        else if (mwatt > 999)
        {
            mwatt = (mwatt - (watt * 1000));
            itoa(mwatt,&mwatt_A1);
            oledPrint(60,3,".",small);
            oledPrint(65,3,mwatt_A1,small);
        }

        // joul weergeven op het scherm
        if (joul < 9)
        {
            itoa(joul,&joul_A1);
            oledPrint(0,4,"Joul:",small);
            oledPrint(50,4,"0",small);
            oledPrint(55,4,"0",small);
            oledPrint(60,4,"0",small);
            oledPrint(65,4,"0",small);
            oledPrint(70,4,joul_A1,small);
        }

        else if (joul < 99)
        {
            itoa(joul,&joul_A1);
            oledPrint(0,4,"Joul:",small);
            oledPrint(50,4,"0",small);
            oledPrint(55,4,"0",small);
            oledPrint(60,4,"0",small);
            oledPrint(65,4,joul_A1,small);
        }

        else if (joul < 999)
        {
            itoa(joul,&joul_A1);
            oledPrint(0,4,"Joul:",small);
            oledPrint(50,4,"0",small);
            oledPrint(55,4,"0",small);
            oledPrint(60,4,joul_A1,small);
        }

        else if (joul < 9999)
        {
            itoa(joul,&joul_A1);
            oledPrint(0,4,"Joul:",small);
            oledPrint(50,4,"0",small);
            oledPrint(55,4,joul_A1,small);
        }

        else if (joul < 99999)
        {
            itoa(joul,&joul_A1);
            oledPrint(0,4,"Joul:",small);
            oledPrint(50,4,joul_A1,small);
        }

        else if (joul > 99999)
        {
            joul = 0;
        }


        //oledWriteBuffer(4,2);

        adc_Sam10();      // Function call for adc_samp
                 // Add all the sampled data and divide by 10 to find average
        adc_A0 = (adc[1]);
        adc_A1 = (adc[0]);
        T = T + 1;

        __delay_cycles(520000);

    }
 }
}
 //ADC10 interrupt service routine
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void)
{
  __bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
}

#pragma vector = PORT2_VECTOR
__interrupt void stop (void)
{
    v = 0;
    P2IFG &= ~(1<<4);           //stop knop

}

// ADC set-up function
void adc_Setup()
{
    ADC10CTL1 = CONSEQ_3 + INCH_1;                      // Repeat sequence-of-channels , A0, A1
    ADC10CTL0 = ADC10SHT_2 + MSC + ADC10ON + ADC10IE;   // Sample & Hold Time + ADC10 ON + Interrupt Enable
    ADC10DTC1 = 0x03;                                   // 3 conversions
    ADC10AE0 |= 0x01;                                   // P1.1 ADC option select
}

// ADC sample conversion function
void adc_Sam10()
{
    ADC10CTL0 &= ~ENC;              // Disable Conversion
    while (ADC10CTL1 & BUSY);       // Wait if ADC10 busy
    ADC10SA = (int)adc;             // Transfers data to next array (DTC auto increments address)
    ADC10CTL0 |= ENC + ADC10SC;     // Enable Conversion and conversion start
    __bis_SR_register(CPUOFF + GIE);// Low Power Mode 0, ADC10_ISR
}
